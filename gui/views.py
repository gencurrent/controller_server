from django.shortcuts import render
from server.models import *
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseBadRequest, HttpResponseServerError
from django.template import loader
# from django.http.response import *

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from server.models import *
from server.serializers import *
import json
import os
from django.conf import settings
from django.views import View
from django.db.models import Q
from django.utils.decorators import method_decorator


@login_required(login_url='/admin/')
def dashboard(request):
    context = {}
    
    clients = Client.active_only().order_by('cid')
    for client in clients:
        client.script_log = client.last_script_log()
    # clients = Client.objects.raw(""" 
    #     SELECT * 
    #     FROM server_client a
    #     LEFT JOIN (
    #         SELECT DISTINCT ON ("script_id") *
    #         FROM server_statuslog
    #         ORDER BY "id" DESC
    #         ) b
    #     ON a.id = b.script_id
    
    # """)
    context.update({
        'clients': clients,
        'commands': get_commands(0)
    })

    template = loader.get_template('dashboard.html')

    return HttpResponse(template.render(context, request))





@csrf_exempt
def commands_view(request, page_number):
    commands = get_commands(page_number)
    serializer = CommandSerializer(commands, many=True)
    return JsonResponse(serializer.data, status=200)

def get_commands(page_number):
    per_page = 400
    ids_from, ids_to = page_number * per_page, page_number * per_page + per_page
    commands = Command.objects.all().order_by('-published')[ids_from:ids_to]
    return commands



@login_required(login_url='/admin/')
def client(request, cid):
    context = {}
    client = Client.objects.get(cid=cid)
    context.update({
        'client': client
    })

    template = loader.get_template('table.html')

    return HttpResponse(template.render(context, request))


@login_required(login_url='/admin/')
def files_management(request):
    context = {}
    fileset = FileSet.objects.latest('timestamp')
    context.update({
        'fileset': fileset
    })

    template = loader.get_template('files_management.html')

    return HttpResponse(template.render(context, request))




@login_required(login_url='/admin/')
def filesets_management(request):
    context = {}
    filesets = FileSet.objects.all().order_by('-timestamp')
    context.update({
        'filesets': filesets
    })

    template = loader.get_template('filesets.html')

    return HttpResponse(template.render(context, request))



def api_client_command(request):
    """
    Метод для создания команды для клиента. 
    JSON клиента:
        -- cid
        -- command
    """
    data = request.POST
    cid = data.get('cid', None)
    command_type = data.get('command', None)

    if not command_type:
        return HttpResponseNotFound('The command param is empty')
    if not cid:
        return HttpResponseNotFound('The CID param is empty')

    try: 
        client = Client.objects.get(cid=cid)

        if command_type == 'update_files':
            last_fileset = FileSet.objects.latest('timestamp')
            for file_in_set in last_fileset.files.all():
                command = Command(client_assigned=client, type='update_file', file=file_in_set)
                command.save()
        else:
            command = Command(client_assigned=client, type=command_type)
            command.save()
    except Exception as e:
        print(e)
        return HttpResponseNotFound('The client with CID %s could not be found' % cid)
    serializer = CommandSerializer(command)
    response = serializer.data
    print(response)
    return JsonResponse(status=200, data=response, safe=False)



def api_mass_command(request):
    """
    Метод для массовой подачи команд клиентам. 
    JSON клиента:
        -- commands - массив клиентов с назначенными командами
            -- cid
            -- command
    """
    print('here')
    data = request.POST
    commands = data.get('commands', [])

    if not commands:
        return HttpResponseNotFound('The commands array is empty param is empty')
    commands = json.loads(commands, encoding='utf-8')

    # try:

    command_list = []
    for command in commands:
        cid = command.get('cid')
        client = Client.objects.get(cid=cid)
        command_type = command.get('command')
        if command_type == 'update_files':
            last_fileset = FileSet.objects.latest('timestamp')
            for file_in_set in last_fileset.files.all():
                command = Command()
                command.client_assigned = client
                command.type = 'update_file'
                command.file = file_in_set
                command.save()
                command_list.append(command)
        else:
            command = Command(client_assigned=client, type=command_type)
            command.save()
            command_list.append(command)
    
    serializer = CommandSerializer(command_list, many=True)
    return JsonResponse(status=200, data=serializer.data, safe=False)


def api_get_clients(request):
    """
        Метод для получения данных о клиентах
    """
    context = {}
    clients = Client.objects.all().order_by('cid')
    context.update({
        'clients': clients
    })

    template = loader.get_template('dashboard.html')
    return HttpResponse(template.render(context, request))



@method_decorator(csrf_exempt, name='dispatch')
class FileView(View):

    def post(self, request, *args, **kwargs):
        uploaded_file = request.FILES.get('files')
        fileset = FileSet.objects.latest('timestamp')

        new_file = open(os.path.join(settings.MEDIA_ROOT, 'client_files/' + uploaded_file.name), 'w+')
        new_file.write(uploaded_file.name)
        new_file = File(file=uploaded_file, original_name=uploaded_file.name)
        new_file.save()
        if not new_file.pk:
            return HttpResponse('Не удалось загрузить файл %s' % (uploaded_file.name), status=500)

        fileset.files.add(new_file)
        fileset.save()
        
        return HttpResponse('Hello', status=200)

    def delete(self, request, *args, **kwargs):
        """
            Метод для удаления файла с сервера
        """
        if not request.method == 'DELETE':
            return HttpResponse('The method is not allowed', status=405)
        data = json.loads(request.body.decode("utf-8", "strict"), encoding='utf-8')
        file_to_delete = data.get('fileid')
        if not file_to_delete:
            return HttpResponseNotFound('The file you have requested could not be found in current fileset')
        try: 
            file_id = int(file_to_delete)
        except: 
            return HttpResponseBadRequest('The fileid parameter must be integer')

        fileset = FileSet.objects.latest('timestamp')
        try:
            fileset.files.filter(pk=file_id).delete()
        except: 
            HttpResponseServerError('Could not delete the the file with id %s' % (file_id))
        response = {'file': file_id}
    
        return JsonResponse(status=200, data=response)


class FilesView(View):
    """
        Класс для обработки списков файлов
    """ 

    def get(self, request, *args, **kwargs):
        """
        Получение списка всех файлов последнего файлсета
        """
        # data = json.laods(request.body, decoding='utf-8')
        
        fileset = FileSet.objects.latest('timestamp')
        files = fileset.files.all()
        context = {}
        files_arr = []
        for filein in files:
            files_arr.append({
                'id': str(filein.pk), 
                'name': filein.get_filename()})
        context['files'] = files_arr
        return JsonResponse(status=200, data=json.dumps(context), safe=False)




@method_decorator(csrf_exempt, name='dispatch')
class FileSetView(View):
    """
        Класс для обработки файлсета
    """ 

    def get(self, request, *args, **kwargs):
        """
        Получение данных текущего файлсета
        """
        return HttpResponseBadRequest('the method not allowed')

    


    def put(self, request, *args, **kwargs):
        """
        Получение списка всех файлов последнего файлсета
        """
        data = json.loads(request.body.decode("utf-8", "strict"), encoding='utf-8')
        fileset_id = data.get('filesetid')
        if not fileset_id:
            return HttpResponse('The fileset id is not in parameters', status=405)
        
        fileset = None
        try: 
            fileset = FileSet.objects.get(pk=fileset_id).touch()
        except:
            return HttpResponse('The could not be found', status=405)

        return HttpResponse(status=203)


@method_decorator(csrf_exempt, name='dispatch')
class FileSetsView(View):
    """
        Класс для обработки списков файлсетов
    """ 

    def get(self, request, *args, **kwargs):
        """
        Получение списка файлсетов
        """
        filesets = FileSet.objects.all().order_by('-timestamp')
        serializer = FileSetSerializer(filesets, many=True)
        return JsonResponse(status=200, data=serializer.data, safe=False)


@method_decorator(csrf_exempt, name='dispatch')
class CommandView(View):
    """
        Класс для обработки списков файлсетов
    """ 

    def get(self, request, *args, **kwargs):
        """
        Получение команды
        """
        uuid = self.kwargs.get('uuid')
        if not uuid:
            return HttpResponseBadRequest('The parameter command uuid is %s' % (uuid))

        try: 
            command = Command.objects.get(uuid=uuid)
        except Command.DoesNotExist as e:
            return HttpResponseNotFound('The command with uuid %s could not be found' % (uuid))
        
        serializer = CommandSerializer(command)
        return JsonResponse(status=200, data=serializer.data, safe=False)
        return HttpResponseNotFound('The command with uuid could not be found')


