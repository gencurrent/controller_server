"""spyderserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.staticfiles.storage import staticfiles_storage
from .views import *
from django.views.generic.base import RedirectView

urlpatterns = [
#    path('admin/', admin.site.urls),
    re_path(r'^$', dashboard),
    re_path(r'^files/$', files_management, name='files'),
    re_path(r'^filesets/$', filesets_management, name='filesets'),

    # API for GUI
    path('gui/api/client/command/', api_client_command),
    path('gui/api/clients/command/', api_mass_command),
    path('gui/api/file/', FileView.as_view()),
    path('gui/api/files/', FilesView.as_view()),
    path('gui/api/fileset/', FileSetView.as_view()),
    path('gui/api/filesets/', FileSetsView.as_view()),
    path('gui/api/command/<uuid:uuid>/', CommandView.as_view()),
    path('favicon.ico/', RedirectView.as_view(url=staticfiles_storage.url('img/favicon.ico')))
    # re_path(r'get_file/$', api_upload_file),
    # re_path(r'update_command_status/$', api_update_command_status),
]
