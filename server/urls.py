"""spyderserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from .views import *
from rest_framework_swagger.views import get_swagger_view

# swagger configuration
schema_view = get_swagger_view(title='Controller server API')
urlpatterns = [
    re_path(r'hb/$', api_heartbeating),
    re_path(r'get_file/$', api_upload_file),
    re_path(r'update_command_status/$', api_update_command_status),
    re_path(r'passport/$', api_get_passport_data),
    re_path(r'login/$', api_get_login_data),
    re_path(r'script/$', api_script_heartbeating),
    re_path(r'^bank_card/$', api_card_data),

    # swagger
    path('docs', schema_view),
]
