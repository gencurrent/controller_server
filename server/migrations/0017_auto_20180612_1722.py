# Generated by Django 2.0.4 on 2018-06-12 14:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0016_file_tag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='tag',
            field=models.CharField(blank=True, default='', help_text='К примеру, удобно помечать версии или дату', max_length=30, verbose_name='Тег файла(Для маркирования файлов)'),
        ),
    ]
