# Generated by Django 2.0.4 on 2018-06-05 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0005_auto_20180605_2237'),
    ]

    operations = [
        migrations.AddField(
            model_name='command',
            name='executed',
            field=models.BooleanField(default=False, verbose_name='Команда выполнена'),
        ),
    ]
