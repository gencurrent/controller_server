# Generated by Django 2.0.4 on 2018-06-19 11:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0023_script_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='slug',
            field=models.CharField(default='', max_length=10, unique=True, verbose_name='Слаг'),
        ),
    ]
