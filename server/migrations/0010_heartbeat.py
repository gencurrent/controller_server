# Generated by Django 2.0.4 on 2018-06-09 16:34

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0009_auto_20180607_0935'),
    ]

    operations = [
        migrations.CreateModel(
            name='HeartBeat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(default=datetime.datetime(2018, 6, 9, 16, 34, 39, 449648, tzinfo=utc), verbose_name='Время харбитинга')),
                ('client', models.ForeignKey(default=-1, on_delete=django.db.models.deletion.CASCADE, to='server.Client')),
            ],
            options={
                'verbose_name_plural': 'Хартбитинги',
                'verbose_name': 'Хартбитинг',
            },
        ),
    ]
