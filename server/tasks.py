from background_task import background
from datetime import timedelta
from django.utils import timezone
from server.models import *
from django.db.models import Q


@background(schedule=60)
def revise_data():
    # lookup user by id and send them a message
    login_data_list = LoginData.objects.filter(Q(active=True) & ~Q(reserved=None))
    for login in login_data_list:
        if login.reserved + timedelta(minutes=90) < timezone.now():
            login.reserved = None
            login.save()
    
    passport_data_list = PassportData.objects.filter(Q(active=True) & ~Q(reserved=None))
    for passport in passport_data_list:
        if passport.reserved + timedelta(minutes=90) < timezone.now():
            passport.reserved = None
            passport.save()



# @background(schedule=120)
# def revise_clients():
#     # lookup user by id and send them a message
#     clients = Client.objects.all()
#     for client in clients:
#         if client.last_heartbeating + timedelta(minutes=2) < timezone.now():
#             client.active = False
#             client.save()
