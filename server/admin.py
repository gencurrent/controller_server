from django.contrib import admin
from .models import *

from django import forms
from django.urls import path
from django.shortcuts import render, redirect
import csv
from django.core.exceptions import ObjectDoesNotExist

# Функции для админки 
def start_clients(modeladmin, request, queryset):
    for client in queryset:
        command = Command()
        command.client_assigned = client
        command.type = 'start'
        command.save()
start_clients.short_description = "Запустить выбранные клиенты"


def stop_clients(modeladmin, request, queryset):
    for client in queryset:
        command = Command()
        command.client_assigned = client
        command.type = 'stop'
        command.save()
stop_clients.short_description = "Остановить выбранные клиенты"

def restart_clients(modeladmin, request, queryset):
    for client in queryset:
        command = Command()
        command.client_assigned = client
        command.type = 'restart'
        command.save()
restart_clients.short_description = "Перезапустить выбранные клиенты"


def update_clients_files(modeladmin, request, queryset):
    last_fileset = FileSet.objects.latest('timestamp')
    for client in queryset:
        for file_in_set in last_fileset.files.all():
            command = Command()
            command.client_assigned = client
            command.type = 'update_file'
            command.file = file_in_set
            command.save()
update_clients_files.short_description = "Обновить файлы последнего файлсета для выбранных клиентов"

def install_requirements(modeladmin, request, queryset):
    for client in queryset:
        command = Command()
        command.client_assigned = client
        command.type = 'inst_reqs'
        command.save()
install_requirements.short_description = "Установить пакеты из requirements.txt"



def activate(modeladmin, request, queryset):
    queryset.update(active=True)
activate.short_description = "Активировать"

def deactivate(modeladmin, request, queryset):
    queryset.update(active=False)
deactivate.short_description = "Деактивировать"


# Классы для админки

class CommandInline(admin.TabularInline):
    model = Command
    extra = 0

class FileInline(admin.TabularInline):
    """
    Инлайн для файлсетов файлов
    """
    model = FileSet.files.through


@admin.register(Client)
class AdminClient(admin.ModelAdmin):
    """
    Админка модели клиентов
    """
    list_display = ['name', 'cid', 'last_beat', 'active', 'ip', ]
    ordering = ['-last_beat',]
    inlines = [CommandInline]

    actions = [start_clients, stop_clients, restart_clients, update_clients_files, install_requirements, activate, deactivate]



@admin.register(Command)
class AdminCommand(admin.ModelAdmin):
    """
    Админка модели команд
    """
    list_display = ['type', 'args', 'client_cid', 'published', 'executed', 'handled']
    date_hierarchy = 'published'
    ordering = ['-published']

    def client_cid(self, obj):
        return obj.client_assigned.cid



@admin.register(File)
class AdminFile(admin.ModelAdmin):
    """
    Админка модели файлов
    """
    list_display = ['file_name', 'tag']
    inlines = [FileInline]

    def file_name(self, obj):
        return obj.get_filename()


@admin.register(HeartBeat)
class AdminHeartBeat(admin.ModelAdmin):
    """
    Админка модели хартбитингов
    """
    list_display = ['client', 'timestamp']



@admin.register(FileSet)
class AdminFileSet(admin.ModelAdmin):
    """
    Админка модели файлсетов
    """
    list_display = ['name', 'timestamp',]
    inlines = [FileInline]
    exclude = ['files']



class CsvImportForm(forms.Form):
    csv_file = forms.FileField()



@admin.register(LoginData)
class AdminLogin(admin.ModelAdmin):
    """
    Админка модели логинов
    """
    list_display = ['login', 'password', 'reserved', 'active',]
    change_list_template = "admin/login/change_list.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file"]
            data = csv_file.read().decode('utf-8').split('\n')
            row_number = 0
            for nrow in data[1:]:
                if not nrow:
                    break
                try: 
                    row_number += 1
                    print('row =')
                    row = nrow.split(',')
                    print(row)
                    login, password = row[0], row[1]
                    try: 
                        login_data = LoginData.objects.get(login=login)
                    except ObjectDoesNotExist as e: 
                        login_data = LoginData(login=login)
                    login_data.password = password
                    login_data.save()
                except:
                    print('Impossible to read the data at %s' % (row_number))
            self.message_user(request, "Успешно импортировано для %s строк" % (row_number))
            return redirect("..")
        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "admin/csv_form.html", payload
        )


@admin.register(PassportData)
class AdminPassport(admin.ModelAdmin):
    """
    Админка модели паспортов
    """
    list_display = ['surname', 'name', 'passport', 'reserved', 'active',]
    change_list_template = "admin/passport/change_list.html"


    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file"]
            data = csv_file.read().decode('utf-8').split('\n')
            row_number = 0
            for nrow in data[1:]:
                if not nrow:
                    break
                try: 
                    row_number += 1
                    print('row =')
                    row = nrow.split(',')
                    print(row)
                    name, surname = row[0], row[1]
                    passport_number = row[2]
                    day, month, year = row[3], row[4], row[5]
                    address, province, city = row[6], row[7], row[8]
                    postal_code, telephone, mobile = row[9], row[10], row[11]
                    try: 
                        passport = PassportData.objects.get(passport=passport_number)
                    except ObjectDoesNotExist as e: 
                        passport = PassportData(passport=passport_number)
                    passport.name = name
                    passport.surname = surname
                    passport.day = day
                    passport.month = month
                    passport.year = year
                    passport.address = address
                    passport.province = province
                    passport.city = city
                    passport.postal_code = postal_code
                    passport.telephone = telephone
                    passport.mobile = mobile
                    passport.save()
                except:
                    print('Impossible to read the data at %s' % (row_number))
            self.message_user(request, "Успешно импортировано для %s строк" % (row_number))
            return redirect("..")
        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "admin/csv_form.html", payload
        )


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ['name', 'color']

@admin.register(Script)
class ScriptAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'client_cid', 'last_beat', 'get_status')

    def client_cid(self, obj):
        return obj.client.cid if obj.client else '------'
    client_cid.short_description = 'CID скрипта'

    def get_status(self, obj):
        status_log = obj.get_last_status_log()
        return status_log.status.name if status_log else '------'
    get_status.short_description = 'Статус скрипта'


@admin.register(StatusLog)
class StatusLogAdmin(admin.ModelAdmin):
    list_display = ('status', 'script',)
    readonly_fields = ('timestamp',)


@admin.register(BankCard)
class BankCardAdmin(admin.ModelAdmin):
    list_display = ['number', 'name', 'month', 'year', 'sms_phone', 'reserved',]

