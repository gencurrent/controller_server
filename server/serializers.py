from rest_framework import serializers
from server.models import *
from datetime import datetime

class ClientSerializer(serializers.ModelSerializer):
    cid = serializers.CharField(read_only=True)
    class Meta:
        model = Client
        fields = ('cid',)

class StatusSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='name', read_only=True)

    class Meta:
        model = Status
        fields = ('name',)

class StatusLogSerializer(serializers.Serializer):
    status = serializers.CharField(source='status__name')

    class Meta:
        model = StatusLog
        fields = ('status',)

class PassportSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=False)
    surname = serializers.CharField(required=False)
    passport = serializers.CharField(required=False)
    day = serializers.CharField(required=False)
    month = serializers.CharField(required=False)
    year = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    province = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    postal_code = serializers.CharField(required=False)
    telephone = serializers.CharField(required=False)
    mobile = serializers.CharField(required=False)

class LoginSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    login = serializers.CharField(required=False)
    password = serializers.CharField(required=False)


class ScriptSerializer(serializers.Serializer):
    uuid = serializers.UUIDField(read_only=True)
    cient = serializers.SlugRelatedField(slug_field='slug', read_only=True)
    last_beat = serializers.CharField(required=False)
    status = serializers.ReadOnlyField(source='get_last_status_name', read_only=True)



class CustomDateTimeField(serializers.Field):
    """
    Color objects are serialized into 'rgb(#, #, #)' notation.
    """
    def to_representation(self, obj):
        timestamp = obj.timestamp.strftime('%Y-%m-%d %H:%M')
        return timestamp


class CustomClientAssignedField(serializers.Field):
    """
    Color objects are serialized into 'rgb(#, #, #)' notation.
    """
    def to_representation(self, obj):
        client_assigned = obj.cid
        return client_assigned


class FileSetSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField()
    timestamp = serializers.DateTimeField(required=False)
    # files = serializers.ReadOnlyField(source='get_last_status_name', read_only=True)



class CommandSerializer(serializers.Serializer):
    
    uuid = serializers.UUIDField(read_only=True)
    type = serializers.CharField(max_length=20)
    args = serializers.CharField()
    published = serializers.DateTimeField(read_only=True)
    handled = serializers.DateTimeField(read_only=True)
    client_assigned = CustomClientAssignedField()
    executed = models.BooleanField()


class BankCardSerializer(serializers.Serializer):
    """
    Сериализатор для банковских карт
    """
    id = serializers.IntegerField(read_only=True)
    number = serializers.CharField(required=False)
    month = serializers.CharField(required=False)
    year = serializers.CharField(required=False)
    name = serializers.CharField(required=False)
    card_cvc = serializers.CharField(required=False)
    sms_phone = serializers.CharField(required=False)
    line = serializers.CharField(required=False)
    reserved = serializers.DateTimeField(required=False)